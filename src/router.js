require("dotenv").config();

import express from "express";
import users from "../data/users";
import { v4 as uuidv4 } from "uuid";
import verifyToken from "./middleware/jwtVerify";
import { readCats, writeCats } from './util/jsonHandler';

const router = express.Router();

let jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");


router.get("/", (req, res) => {
  return res.status(200).send("Hello World");
});

router.get("/cats", verifyToken, (req, res) => {
  readCats().then((catsJson) => {
    return res.status(200).send(catsJson);
  })

});

router.post("/cats", (req, res) => {
  let newCat = {
    id: uuidv4(),
    name: req.body.name,
    breed: req.body.breed,
  };
  
  readCats().then((catsJson) => {
    catsJson.push(newCat);
    writeCats(catsJson);
    return res.status(201).send(catsJson);
  });
  
});

router.post("/register", (req, res) => {
  let newUser = {
    email: req.body.email,
    password: req.body.password,
  };

  bcrypt.hash(newUser.password, `${process.env.saltRounds}`, function (err, hash) {
    // Store hash in your password DB.
    newUser.password = hash;
    let token = jwt.sign(newUser, `${process.env.privateKey}`);
    users.push(newUser);
    return res.status(201).send(token);
  });
});

router.get("*", (req, res, next) => {
  let err = new Error("typed wrong URL");
  next(err);
});

export default router;
